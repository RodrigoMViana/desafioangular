import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { RequestUpdate } from '../user.model';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  [x: string]: any;

  id:string;
  request: RequestUpdate;

  //constructor(private userService: UserService, private ActivatedRoute) { }

  ngOnInit(): void {
   //this.id =this.route.snapshot.paramMap.get('id');
    //  this.userService.getUser(this.id).subscribe(res => {
   // this.request = {
    // name: '$(resizeBy.data.nome) $(res.data.adim) $(res.data.email) $(res.data.senha) $(res.data.login) $(res.data.id)'
    }
    // });
    
    update() {
      this.userService.updateUser(this.id, this.request).subscribe(res => {
        alert('Atualizar: ${res.updateAt}, Nome: ${res.nome}, ${res.email} $(res.senha} ${res.login} ${res.id} ');
        this._route.navigate(['/users']);
      })
      }

}
