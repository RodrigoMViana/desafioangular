export interface User {
    admin: boolean;
    email: string;
    id: number;
    login: string;
    nome: string;
}

export interface ResponseUsers {
    type: string;
    properties: User;
    title: string;
}

    export interface RequestCreate {
        email: string;
        login: string;
        nome: string;
        senha: string;
        }
    

    export interface ResponseCreate {
        id: number;
        nome: string;
        email: string;
        login: string;
        admin: boolean;
        }

  //modelos para o get user  

    export interface ResponseUser {
        properties: User
    }

    export interface RequestUpdate {
        admin: boolean,
        email: "string",
        id: number,
        login: "string",
        nome: "string",
        senha: "string"
        }
    

    export interface ResponseUpdate {
        id: number;
        nome: string;
        email: string;
        login: string;
        admin: boolean;
        }
